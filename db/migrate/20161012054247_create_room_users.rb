class CreateRoomUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :room_users do |t|
      t.integer :room_id, null: false
      t.integer :user_id, null: false
      t.timestamps
    end
  end
end
