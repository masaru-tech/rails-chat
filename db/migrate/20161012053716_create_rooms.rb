class CreateRooms < ActiveRecord::Migration[5.0]
  def change
    create_table :rooms do |t|
      t.string :name, null: false
      t.boolean :private, null: false, default: false
      t.timestamps

      t.index ['name'], name: 'index_rooms_name', unique: true, using: :btree
    end
  end
end
