class CreateChatMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :chat_messages do |t|
      t.text :content, null: false
      t.integer :user_id, null: false
      t.timestamps
    end

    add_foreign_key :chat_messages, :users, name: 'chat_messages_user_id_fk'
  end
end
