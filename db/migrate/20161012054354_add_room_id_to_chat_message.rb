class AddRoomIdToChatMessage < ActiveRecord::Migration[5.0]
  def change
    add_column :chat_messages, :room_id, :integer, null: false, default: 0
  end
end
