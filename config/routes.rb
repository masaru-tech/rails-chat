Rails.application.routes.draw do
  devise_for :users, :controllers => {sessions: 'sessions', registrations: 'registrations'}
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :rooms, param: :room_name do
    member do
      get 'messages'
    end
  end

  # Serve websocket cable requests in-process
  mount ActionCable.server => '/cable'
end
