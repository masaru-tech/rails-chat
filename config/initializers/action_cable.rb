Rails.application.config.action_cable.disable_request_forgery_protection = Rails.env.development? || Rails.env.test?
