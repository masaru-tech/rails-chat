class Room < ApplicationRecord
  has_many :room_users

  scope :public_room, -> { where(private: false) }
  scope :private_room_member, ->(current_user) { joins(:room_users).where(private: true, room_users: {user_id: current_user.id}) }

  def to_param
    return name
  end
end
