class ChatMessage < ApplicationRecord
  belongs_to :user
  belongs_to :room

  after_create_commit { MessageBroadcastJob.perform_later(self) }

  def own?(current_user)
    self.user_id == current_user.id
  end

  def markdown
    html_render = Redcarpet::Render::HTML.new(filter_html: true, hard_wrap: true)
    options = {
      autolink: true,
      fenced_code_blocks: true,
      hard_wrap: true,
    }
    markdown = Redcarpet::Markdown.new(html_render, options)
    emojify(markdown.render(self.content))
  end

  private
  def emojify(content)
    content.gsub(/:([\w+-]+):/) do |match|
      if (emoji = Emoji.find_by_alias($1))
        %(<img alt="#$1" src="#{ActionController::Base.helpers.image_path("emoji/#{emoji.image_filename}")}" style="vertical-align:middle" width="20" height="20" />)
      else
        match
      end
    end if content.present?
  end
end
