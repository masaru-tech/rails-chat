# textareaの高さ自動調整
$(document).on('input', '#chat-message', (e)->
  new_lines = $(this).val().match(/\r\n|\n/g)
  if new_lines == null
    $(e.target).css('height', '34px')
  else
    $(e.target).css('height', (34 + (new_lines.length * 17)) + 'px')
)

$(document).on('keypress', '#chat-message', (e) ->
  if e.keyCode is 13 && !e.shiftKey
    App.room.speak e.target.value
    e.target.value = ''
    $(this).trigger('input')
    e.preventDefault()
)