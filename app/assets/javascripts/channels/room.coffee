jQuery(document).on 'turbolinks:load', ->
  $.AdminLTE.layout.activate()
  messages = $('.direct-chat-messages')

  messages_to_bottom = ->
    $('html,body').scrollTop(messages.height())

  App.room = App.cable.subscriptions.create {channel: "RoomChannel", room_name: messages.data('room-name')},
    connected: ->
      # Called when the subscription is ready for use on the server

    disconnected: ->
      # Called when the subscription has been terminated by the server

    received: (data) ->
      # Called when there's incoming data on the websocket for this channel
      message_list = new ChatMessageList
      message_list.add(data['message'])
      messages_to_bottom()

    speak: (message)->
      @perform 'speak', message: message

  $.ajax({
    type: 'GET',
    url: "#{messages.data('room-name')}/messages",
    dataType: 'json',
  }).done((response, textStatus, jqXHR)->
    message_list = new ChatMessageList
    for message in response.messages
      message_list.add(message)
    messages_to_bottom()
  )
