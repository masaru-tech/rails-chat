class @Utils
  @relativeDate = (date)->
    now = new Date()
    diff = (now.getTime() - date.getTime()) / 1000

    switch true
      when diff < 60
        "たった今"
      when diff < 120
        "約 1分前"
      when diff < 3600
        "約" + Math.floor(diff / 60) + "分前"
      when diff < 7200
        "約 1時間前"
      when diff < 86400
        "約" + Math.floor(diff / 3600) + "時間前"
      when diff < 604800
        "約" + Math.floor(diff / 86400) + "日前"
      else
        "約" + Math.floor(diff / 604800) + "週間前"

  @updateCreatedAt = ()->
    for created_at_ele in $('.direct-chat-timestamp')
      created_at = new Date($(created_at_ele).data('created-at'))
      $(created_at_ele).html "(" + @relativeDate(created_at) + ")"

setInterval(()->
  @Utils.updateCreatedAt()
, 1000)