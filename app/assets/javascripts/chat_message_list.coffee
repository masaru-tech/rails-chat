class @ChatMessageList
  add: (message)->
    $('.direct-chat-messages').append JST['templates/chat_message']({message: message, current_user: gon.current_user})