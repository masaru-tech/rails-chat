class SidemenuCell < Cell::ViewModel
  include Cell::Slim
  include ApplicationHelper
  include Devise::Controllers::Helpers

  def show
    @public_rooms = Room.public_room
    @private_rooms = Room.private_room_member(current_user)

    render
  end

end
