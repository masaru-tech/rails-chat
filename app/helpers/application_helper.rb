module ApplicationHelper
  def active_class(room_name)
    request.path.split('/').last == room_name ? 'active' : nil
  end
end
