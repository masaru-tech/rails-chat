module ChatView
  class SiteCardSerializer
    attr_reader :url

    def initialize(url)
      @url = url

      charset = nil
      html = open(url) do |f|
        charset = f.charset # 文字種別を取得
        f.read # htmlを読み込んで変数htmlに渡す
      end

      # ノコギリを使ってhtmlを解析
      @doc = Nokogiri::HTML.parse(html, charset)
    end

    def title
      if @doc.css('//meta[property="og:site_name"]/@content').empty?
        return @doc.title.to_s
      else
        return @doc.css('//meta[property="og:site_name"]/@content').to_s
      end
    end

    def description
      if @doc.css('//meta[property="og:description"]/@content').empty?
        return @doc.css('//meta[name$="escription"]/@content').to_s
      else
        return @doc.css('//meta[property="og:description"]/@content').to_s
      end
    end

    def image
      if @doc.css('//meta[property="og:image"]/@content').empty?
        # 画像が取得できない場合はgoogle API(非公式)でfaviconを取得する
        return "http://www.google.com/s2/favicons?domain=#{self.url.gsub(/(http|https):\/\//, "").split("/").first}"
      else
        return @doc.css('//meta[property="og:image"]/@content').to_s
      end
    end

    def as_json(options={})
      {
        url: self.url,
        title: self.title,
        description: self.description,
        image: self.image
      }
    end
  end
end