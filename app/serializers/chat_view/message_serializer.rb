module ChatView
  class MessageSerializer
    def initialize(message)
      @message = message
    end

    def as_json(options={})
      links = @message.content.split(/(\s|\r\n|\n)/).find_all{|msg| msg =~ /^(http|https)/}
      {
        content: @message.markdown,
        created_user_id: @message.user.id,
        created_user_name: @message.user.name,
        created_at: @message.created_at,
        links: links.map{|link| ::ChatView::SiteCardSerializer.new(link)}
      }
    end
  end
end