class RoomsController < ApplicationController
  before_action :authenticate_user!

  def show
    gon.current_user = {
      id: current_user.id
    }

    @room = Room.find_by(name: params[:room_name])

    render template: "chat_view/index"
  end

  def messages
    if request.format.json?
      room = Room.find_by(name: params[:room_name])
      render json: {messages: ChatMessage.where(room_id: room.id).includes(:user).map{|message| ::ChatView::MessageSerializer.new(message)}}
    end
  end
end
