class SessionsController < Devise::SessionsController
  layout 'login'

  def destroy
    gon.clean
    super
  end
end
