class MessageBroadcastJob < ApplicationJob
  queue_as :default

  def perform(message)
    ActionCable.server.broadcast("room_#{message.room.name}", message: ::ChatView::MessageSerializer.new(message).as_json)
  end
end
